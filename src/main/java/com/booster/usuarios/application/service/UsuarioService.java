package com.booster.usuarios.application.service;

import com.booster.usuarios.domain.Usuario;
import org.springframework.stereotype.Service;

@Service
public interface UsuarioService {
  public boolean newUser(Usuario usuario);
  public Usuario getUser (Long Id);
}
