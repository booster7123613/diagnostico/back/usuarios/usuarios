package com.booster.usuarios.infrastructure.client.dto;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UsuarioDto {
  public Long Id;
  public String Nombre;
  public String ApellidoPaterno;
  public String ApellidoMaterno;
}
