package com.booster.usuarios.infrastructure.client.implementation;

import com.booster.usuarios.application.service.UsuarioService;
import com.booster.usuarios.domain.Usuario;
import com.booster.usuarios.infrastructure.db.entity.UsuarioEntityMapper;
import com.booster.usuarios.infrastructure.db.jpa.UsuarioJpaImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServiceImpl implements UsuarioService {

  @Autowired
  private UsuarioJpaImpl usuarioJpaImpl;

  @Autowired
  private UsuarioEntityMapper usuarioEntityMapper;
  @Override
  public boolean newUser(Usuario usuario) {
    return false;
  }

  @Override
  public Usuario getUser(Long Id) {
    return usuarioEntityMapper.toDomain(
        usuarioJpaImpl.getUser(Id));
  }


}
