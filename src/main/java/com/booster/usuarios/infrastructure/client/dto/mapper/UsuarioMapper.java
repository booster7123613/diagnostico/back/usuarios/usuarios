package com.booster.usuarios.infrastructure.client.dto.mapper;

import com.booster.usuarios.domain.Usuario;
import com.booster.usuarios.infrastructure.client.dto.UsuarioDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UsuarioMapper {
  UsuarioDto toDto(Usuario source);
}
