package com.booster.usuarios.infrastructure.db.jpa.repository;

import com.booster.usuarios.infrastructure.db.entity.UsuarioEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuariosRespository extends JpaRepository<UsuarioEntity,Long> {

}

