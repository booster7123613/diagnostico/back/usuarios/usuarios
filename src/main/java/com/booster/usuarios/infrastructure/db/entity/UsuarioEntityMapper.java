package com.booster.usuarios.infrastructure.db.entity;

import com.booster.usuarios.domain.Usuario;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UsuarioEntityMapper {

  Usuario toDomain(UsuarioEntity source);

  UsuarioEntity toEntity(Usuario source);
}
