package com.booster.usuarios.infrastructure.rest.spring;

import com.booster.usuarios.application.service.UsuarioService;
import com.booster.usuarios.infrastructure.client.dto.UsuarioDto;
import com.booster.usuarios.infrastructure.client.dto.mapper.UsuarioMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RequestMapping("/endpoint")
@RestController
public class UsuariosController {

  @Autowired(required = true)
  private UsuarioService usuarioService;
  private UsuarioMapper usuarioMapper;
  @GetMapping("/")
  public UsuarioDto obtenerSesiones() {
    return usuarioMapper.toDto(usuarioService.getUser(1L));
  }

}
