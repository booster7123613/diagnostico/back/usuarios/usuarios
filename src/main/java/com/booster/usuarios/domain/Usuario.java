package com.booster.usuarios.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Usuario {
  public Long Id;
  public String Nombre;
  public String ApellidoPaterno;
  public String ApellidoMaterno;
}
